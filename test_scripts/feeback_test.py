"""
Retract the first actuator initialized.  
Then extend the actuator, stopping when FSR feedback exceeds a threshold.
Retract again when fully extended.
"""
import time
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from motor_control import lac
from data_collection import readADC

lacs = lac.setupMultiple()
adc = readADC.ADC()

lacs[1].set_accuracy(4)
while True:
    # retract actuator to start point
    lacs[1].set_speed(400)
    lacs[1].set_position(100)
    time.sleep(10)

    # start moving to endpoint
    lacs[1].set_speed(200)
    lacs[1].set_position(900)
    cur_pos = lacs[1].get_feedback()
    
    touching = False
    while cur_pos < 895:
        # stop the motor when it puts pressure on the resistor
        if adc.channel[1].value > 3000 and not touching:
            lacs[1].set_position(cur_pos)
            touching = True
            print('Touching trigger', adc.channel[1].value)
            time.sleep(5)
            lacs[1].set_position(900)

        while adc.channel[1].value > 9000 and touching:
            lacs[1].set_position(cur_pos)
            print('Pulling trigger', adc.channel[1].value)

        # reset position to end, and report current position
        lacs[1].set_position(900)
        time.sleep(0.1)
        cur_pos = lacs[1].get_feedback()
        print(cur_pos, adc.channel[1].value)
