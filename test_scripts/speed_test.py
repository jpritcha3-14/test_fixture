"""
Tests the first actuator encountered by recording the a timestamp and position as
the actuator extends and retracts at different speeds.
"""
import time
import os
import sys

from itertools import chain

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from motor_control import lac

dir_name = 'speed_test_results'
dir_path = os.path.join(os.getcwd(), dir_name) 
file_name = 'speed_test'
file_path = os.path.join(dir_path, file_name)
if not os.path.exists(dir_path):
    os.mkdir(dir_path)
if os.path.exists(file_path):
    os.remove(file_path)
    
def write_output(speed, values, extend=True):
    with open(os.path.join(dir_path, file_name), "a") as fd:
        for val in values:
            fd.write(','.join(map(str, chain(val, [speed, int(extend)]))) + '\n')

lacs = lac.setupMultiple()

a = lacs[0]
a.set_accuracy(4)
start_speed = 100
start_pos = 80
end_speed = 800
end_pos = 920
step = 50
delta = 20
cur_speed = start_speed

# Initialize actuator
a.set_speed(500)
a.set_position(start_pos)
time.sleep(15)
cur_pos = a.get_feedback()

while cur_speed <= end_speed:
    extending = [] 
    print('Extending at speed {}'.format(cur_speed))
    a.set_speed(cur_speed)
    a.set_position(end_pos)
    start_time = time.time_ns() 

    while cur_pos < end_pos - delta:
        time.sleep(0.1)
        cur_pos = a.get_feedback()
        extending.append((time.time_ns() - start_time, cur_pos))

    write_output(cur_speed, extending, True)
    time.sleep(3)

    retracting = [] 
    print('Retracting at speed {}'.format(cur_speed))
    a.set_position(start_pos)
    start_time = time.time_ns() 

    while cur_pos > start_pos + delta:
        time.sleep(0.1)
        cur_pos = a.get_feedback()
        retracting.append((time.time_ns() - start_time, cur_pos))

    write_output(cur_speed, retracting, False)
    cur_speed += step
    time.sleep(3)

print('Finished testing speeds of {} to {} with a step of {}'.format(start_speed, end_speed, step))
print('Results written to {}'.format(os.path.join(dir_path, file_name)))
