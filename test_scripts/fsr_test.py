"""
Continuously read values from ADC0 and ADC1
"""

import time
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from data_collection import readADC

adc = readADC.ADC()

while True:
    print('ADC0: ', adc.channel[0].value)
    print('ADC1: ', adc.channel[1].value)
    time.sleep(1)
