"""
Retract the first actuator initialized.  
Then extend by 5 units each time enter is pressed.
Retract again when fully extended.
"""
import time
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from motor_control import lac

lacs = lac.setupMultiple()

a = lacs[0]
a.set_accuracy(4)
while True:
    cur_pos = 100

    a.set_speed(800)
    a.set_position(100)
    time.sleep(10)
    print('ready')
    a.set_speed(400)

    while cur_pos < 895:
        input()
        cur_pos += 5
        a.set_position(cur_pos)
        print('extending: {}'.format(cur_pos))
