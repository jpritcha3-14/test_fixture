## Introduction
This repo contains code for the high-level interface to the test fixture, using feedback from a pressure sensitive resistor to control actuators.

## Setup
```
git clone --recurse-submodules https://jpritcha3-14@bitbucket.org/jpritcha3-14/test_fixture.git
```

Please follow the setup instructions in these repos for both hardware and dependencies:

- [motor_control](https://bitbucket.org/jpritcha3-14/motor_control)
- [data_collection](https://bitbucket.org/jpritcha3-14/data_collection)

Using a [virtual environment](https://docs.python.org/3/tutorial/venv.html) can help keep things clean and bundled.

The requirements.txt file can be uesed to set up the virtual environemnt with all the needed packages:
```
cd /path/to/test_fixture
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

The full system hardware setup is shown in the diagram below:

![system diagram](system_diagram.png)

## Setting up Raspberry Pi
Please [click here](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up) to follow the set up instructions for Raspberry Pi. 

## Usage
Currently the feedback_test.py script tests a single actuator and pressure sensitive resistor.  It moves the actuator in and out repeatedly, stopping and starting as pressure is applied to the pressure sensitive resistor. 

